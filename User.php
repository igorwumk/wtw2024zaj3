<?php
namespace WTW2024zaj3;

class User
{
    public string $name;
    public string $surname;
    public string $username;

    protected bool $is_admin = false;

    function __construct(string $value1, string $value2, string $value3)
    {
        $this->name = $value1;
        $this->surname = $value2;
        $this->username = $value3;
    }

    function isAdmin()
    {
        return $this->is_admin;
    }

    function printFullName()
    {
        
        if($this->isAdmin())
        {
            print $this->name . " " . $this->surname . " (admin)" . PHP_EOL;
        }
        else
        {
            print $this->name . " " . $this->surname . PHP_EOL;
        }
    }
}