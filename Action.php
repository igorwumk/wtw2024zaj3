<?php
namespace WTW2024zaj3;
require 'User.php';
require 'Admin.php';
require 'Client.php';

$user = new User("Francis", "Pagano", "FrPa21");
$user->printFullName();

$admin = new Admin("Maximo", "Wheetington", "thewheel");
$admin->printFullName();

$client = new Client("Lanny", "Destefanis", "lannylannie");
$client->setCity("Columbia");
$client->setState("MO");
$client->setCountry("US");
$client->printFullName();
$client->location();
