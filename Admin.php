<?php
namespace WTW2024zaj3;

class Admin extends User
{
    function __construct(string $value1, string $value2, string $value3)
    {
        parent::__construct($value1, $value2, $value3);
        $this->is_admin = true;
    }
}