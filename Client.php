<?php
namespace WTW2024zaj3;

class Client extends User
{
    private string $city;
    private string $state;
    private string $country;

    function __construct(string $value1, string $value2, string $value3)
    {
        parent::__construct($value1, $value2, $value3);
    }

    function setCity(string $value)
    {
        $this->city = $value;
    }
    function getCity()
    {
        return $this->city;
    }

    function setState(string $value)
    {
        $this->state = $value;
    }
    function getState()
    {
        return $this->state;
    }

    function setCountry(string $value)
    {
        $this->country = $value;
    }
    function getCountry()
    {
        return $this->country;
    }

    function location()
    {
        print $this->city . ", " . $this->state . ", " . $this->country . PHP_EOL;
    }
}